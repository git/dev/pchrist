# Copyright 1999-2015 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI=5

inherit eutils git-r3

DESCRIPTION="Freetalk is a console based chat client for Google Hangout and other XMPP servers"
HOMEPAGE="http://gnufreetalk.github.io/"
EGIT_REPO_URI="https://github.com/GNUFreetalk/freetalk.git"
LICENSE="GPL-3"
SLOT="0"
KEYWORDS=""
DEPEND=">=dev-scheme/guile-2.0.0
			net-libs/loudmouth"
RDEPEND="${DEPEND}"
IUSE="examples"

src_prepare() {
	./autogen.sh || die "Autogen script failed."
}

src_install() {
	emake DESTDIR="${D}" install
	dodoc NEWS README TODO ChangeLog
	local baddoc="${D}/usr/share/doc/${PN}"
	if use examples; then
		for f in $(find  "${baddoc}/examples" -type f);
		do
			docinto examples;
			dodoc "${f}"
		done
	fi
	rm -Rv "${baddoc}"
}
