# Copyright 1999-2011 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: /var/cvsroot/gentoo-x86/dev-lang/swig/swig-2.0.4.ebuild,v 1.1 2011/05/28 15:40:26 pchrist Exp $

EAPI="3"

MY_P="GeographicLib-${PV}"
DESCRIPTION="A small C++ library for geodetic calculations"
HOMEPAGE="http://geographiclib.sourceforge.net/"
SRC_URI="mirror://sourceforge/${PN}/${MY_P}.tar.gz"

LICENSE="LGPL-3"
SLOT="0"
KEYWORDS="~x86 ~amd64"
IUSE=""

DEPEND=""
RDEPEND=""

S="${WORKDIR}/${MY_P}"

#src_configure() {
#	econf
#}
#

# test for signed manifest, pchrist 03Mar13

src_install() {
	emake DESTDIR="${D}" install || die "emake install failed"
	dodoc AUTHORS NEWS 00README.txt INSTALL || die "dodoc failed"
}
