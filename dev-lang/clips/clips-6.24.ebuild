# Copyright 1999-2008 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

#inherit eutils flag-o-matic autotools

DESCRIPTION="A Tool for Building Expert Systems"
HOMEPAGE="http://clipsrules.sourceforge.net/"

MY_PV="${PV/./}"
MY_PN="${PN}rules"
MY_PN2="${PN}_core_source"

SRC_URI="mirror://sourceforge/${MY_PN}/${MY_PN2}_${MY_PV}.tar.Z
		mirror://sourceforge/${MY_PN}/make_and_help_files_${MY_PV}.zip
		X?	( mirror://sourceforge/${MY_PN}/x_windows_ide_source_${MY_PV}.tar.Z )"

LICENSE="as-is"
SLOT="0"

KEYWORDS="~x86"
IUSE="X"

DEPEND="app-arch/unzip
		sys-libs/ncurses
		X? ( x11-base/xorg-server )"
RDEPEND="${DEPEND}"

S="${WORKDIR}/${PN}src/${PN}src"

src_unpack() {
	pwd
	unpack ${A}
	cd "${S}"
	cp -v ../../makefile.gcc Makefile
	sed -i -e 's/-ltermcap/-lncurses $(CFLAGS) /' Makefile
	sed -i -e 's/-Wno-implicit/-Wno-implicit $(CFLAGS) /' Makefile
	if use X; then
		pushd ../../x-prjct/makefile
			sed -i -e 's/$(INCLUDES)/$(CFLAGS)/' makefile.x
			sed -i -e 's/$(LDFLAGS)/$(CFLAGS)/' makefile.x
		popd
		mkdir -v xclips
		cp -v * xclips
		cp -v ../../x-prjct/makefile/makefile.x xclips/Makefile
		cp -v ../../x-prjct/color/* xclips
		cp -v ../../x-prjct/xinterface/* xclips
	fi
}

src_compile() {
	make clips
	if use X; then
		cd xclips
		make xclips
	fi
}

src_install() {
	exeinto /usr/bin
	doexe clips
	doexe xclips/xclips
}
