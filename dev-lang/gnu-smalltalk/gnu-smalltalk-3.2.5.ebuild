# Copyright 1999-2013 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI="3"

inherit elisp-common flag-o-matic eutils autotools multilib

DESCRIPTION="GNU Smalltalk"
HOMEPAGE="http://smalltalk.gnu.org"
SRC_URI="mirror://gnu/smalltalk/smalltalk-${PV}.tar.gz"
LICENSE="GPL-2"
SLOT="0"
# don't forget to keyword on more arches ???
KEYWORDS="~amd64"
#IUSE="tk readline emacs gtk X jit disassembler"
IUSE="tk readline emacs gtk X disassembler"

	#app-arch/zip
	#sys-libs/gdbm
	#sys-apps/debianutils
	#virtual/libffi
	#emacs? ( virtual/emacs )
	#readline? ( sys-libs/readline )
	#tk? ( dev-lang/tk )
	#gtk? ( =x11-libs/gtk+-2* )
	#gmp? ( dev-libs/gmp )

	# zlib????

DEPEND="
	app-arch/zip
	${RDEPEND}
"

	#sys-devel/libtool
	#sys-libs/gdbm
	#sys-libs/zlib
	#dev-libs/expat

RDEPEND="
	dev-libs/libsigsegv
	virtual/libffi
	sys-devel/libtool
	sys-libs/gdbm
	sys-libs/zlib
	dev-libs/expat
	dev-libs/nettle
	net-libs/gnutls
	dev-libs/gmp
	readline? ( sys-libs/readline sys-libs/ncurses )
"

# TODO:
# need to check dependencies and automagic stuff
# need to check all flags. With tk on it fails to compile.
# need to check what is the old patch (3.2.4)
# pchrist, 01Jul13

S="${WORKDIR}/smalltalk-${PV}"

SITEFILE=50gnu-smalltalk-gentoo.el

#src_prepare() {
#	epatch "${FILESDIR}"/SequenceableCollection-size-3.2.4.patch
#}


src_prepare() {
	eautoreconf
}

src_configure() {

	replace-flags '-O3' '-O2'

		#--libdir=/usr/$(get_libdir) 
		#--disable-jit: when ON fails to compile at least on ~amd64

	econf \
		--with-system-libsigsegv \
		--with-system-libffi \
		--with-gmp \
		--disable-jit \
		$(use_with emacs emacs) \
		$(use_with readline readline) \
		$(use_with tk tcl /usr/$(get_libdir)) \
		$(use_with tk tk /usr/$(get_libdir)) \
		$(use_with X x) \
		$(use_enable disassembler) \
		$(use_enable gtk)
}

src_compile() {
	emake || die "emake failed"
	use emacs && elisp-compile *.el
}

src_install() {
	emake DESTDIR="${D}" install || die
	dodoc AUTHORS COPYING* ChangeLog NEWS README THANKS TODO
	if use emacs; then
		elisp-install "${PN}" *.el *.elc
		elisp-site-file-install "${FILESDIR}/${SITEFILE}"
	fi
	fperms 0444 /usr/share/smalltalk/packages.xml
}

pkg_postinst() {
	use emacs && elisp-site-regen
}

pkg_postrm() {
	use emacs && elisp-site-regen
}
